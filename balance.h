#ifndef BALANCE_H
#define BALANCE_H

#include <string>
#include <vector>
#include <iostream>

enum class tableEn {
    COSTS = 0,
    INSTITYT,
    TRANSPORT,
    ENTERTAINMENT,
};

typedef unsigned int uint;

namespace myClass {

    enum class tableEn {
        COSTS = 0,
        INSTITYT,
        TRANSPORT,
        ENTERTAINMENT,
     };

	class balance{
        private:
            std::string _filder = "";
            std::vector<std::vector<std::string> > caffeCinema;
            std::vector<std::vector<std::string> > costs;
            std::vector<std::vector<std::string> > institute;
            std::vector<std::vector<std::string> > transport;
            uint workday[12] = {16, 19, 22, 21, 18, 21, 21, 23, 22, 21, 21, 21};
            uint getDaysCount(uint month) const;
            bool isMonth(int month) const;
        public:

            balance() = default;
            balance(std::string filder);
            bool initDatabase();
            uint getOtherMontlyCosts(const std::string& city, uint age) const;
            uint getInstituteDinnerCost(const std::string& city, const std::string& institute) const ; //+
            uint getCinemaCost(const std::string& city, const std::string& cinema) const;
            uint getCoffeeCost(const std::string& city, const std::string& coffee) const;
            uint getHomeFoodCost(const std::string& city) const;
            uint getWeekandCost(const std::string& city, const std::string& cinema, const std::string& coffee)  const;
            uint getTransportCost(const std::string& city, const std::string& homeAddress, const std::string& institute) const;
            uint getWorkdayCost(const std::string& city, const std::string& homeAddress,
				    const std::string& institute) const;
            uint getCosts(uint month, const std::string& city, const std::string& homeAddress,
				    const std::string& institute, const std::string& cinema,
				    const std::string& coffee, uint age) const;

            bool isCity(const std::string& city) const;
            bool getInstitut(const std::string& city, const std::string& instity) const;
            bool getCinema(const std::string& city, const std::string& cinema) const;
            bool getCoffe(const std::string& city, const std::string& coffe) const;
            uint getMonth() const;
            uint getAge() const;
            uint getWorkdays(uint month) const;
            std::string getCity() const;
            bool getAddress(const std::string& city, const std::string& address) const;
            void saveFile() ;
            const std::vector<std::vector<std::string> >& getArrCafeCinema() const;
            const std::vector<std::vector<std::string> >& getArrCosts() const;
            const std::vector<std::vector<std::string> >& getArrInstitute() const;
            const std::vector<std::vector<std::string> >& getArrTransport() const;
            bool saveVector(std::vector<std::vector<std::string> >& data, myClass::tableEn newWind);
	};
};

#endif // BALANCE_H

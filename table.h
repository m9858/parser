#ifndef TABLE_H
#define TABLE_H

#include <QDialog>
#include <balance.h>


namespace Ui {
	class Table;
}

class Table : public QDialog {
	    Q_OBJECT

	public:
	    explicit Table(QWidget *parent = nullptr);
	    void exitTable(myClass::balance* balans, myClass::tableEn tab);
	    ~Table();

	private slots:
	    void on_pushButton_clicked();

	    void on_pushButton_2_clicked();

	    void on_pushButton_3_clicked();

	private:
	    myClass::balance* balans;
	    Ui::Table *ui;
	    myClass::tableEn openWindow;
	    std::vector<std::string> rootTable;
	    int notVoidElementTabel;
};

#endif // TABLE_H

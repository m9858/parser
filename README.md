# Parser as a laboratory work on MISP

## About the project

The code is written in C++ and with the qt framework and using qmake.
There are 5 fields in the code with ***costs, institutions, transport, entertainment and
 related ones***, they have buttons that calculate according to formulas, calculates and searches and displays, there is also a change button in the add table, delete rows and change elements.

## Work how to open file

Select in the code a folder with such files in the project they are in the ***fileCsv***

```
    Caffe-and-cinema.csv  
    Costs.csv  
    Institute.csv  
    Transport.csv
```

All of these should be in a file with this name!!!

## Launch

Well, we don’t consider launching through the idle, but the main thing is that it should be qt.
Run through the Linux terminal, go to the folder with the project.

``` 
    qmake
    make
    ./Parser
```

The main thing is to have C++ and a library Qt

#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow root;
    root.setWindowTitle("Parser");
    root.show();
    return a.exec();
}

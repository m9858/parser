#include "table.h"
#include "ui_table.h"

#include <QStringList>
#include <QMessageBox>

Table::Table(QWidget *parent) : QDialog(parent), ui(new Ui::Table) {
    this->notVoidElementTabel = 0;
    this->balans = nullptr;
    ui->setupUi(this);
}

void Table::exitTable(myClass::balance* balans, myClass::tableEn tab) {
    this-> openWindow = tab;
    this->balans = balans;
    QStringList tout;
    std::vector<std::vector<std::string> > tmp;
    switch (tab) {
        case (myClass::tableEn::COSTS):
            this->setWindowTitle("Table Costs");
            this->ui->label->setText("Table Costs");
            tmp = this->balans->getArrCosts();
            break;
        case (myClass::tableEn::INSTITYT):
            this->setWindowTitle("Table Instityt");
            this->ui->label->setText("Table Instityt");
            tmp = this->balans->getArrInstitute();
            break;
        case (myClass::tableEn::TRANSPORT):
            this->setWindowTitle("Table Transport");
            this->ui->label->setText("Table Transport");
            tmp = this->balans->getArrTransport();
            break;
        case (myClass::tableEn::ENTERTAINMENT):
            this->setWindowTitle("Table Caffee and Cinema");
            this->ui->label->setText("Table Caffee and Cinema");
            tmp = this->balans->getArrCafeCinema();
            break;
    }
    this->ui->spinBox->setMaximum(tmp.size() - 1);
    this->rootTable = tmp[0];
    this->ui->tableWidget->setColumnCount(tmp[0].size());
    this->ui->tableWidget->setRowCount(tmp.size() - 1);
    for (size_t i = 0; i < tmp[0].size(); i++) {
        tout << QString::fromStdString(tmp[0][i]);
        if (tmp[0][i].size()) {
            this->notVoidElementTabel = i;
        }
    }
    this->notVoidElementTabel += 1;
    this->ui->tableWidget->setHorizontalHeaderLabels(tout);
    for (size_t i = 1; i < tmp.size();  i++) {
        for (size_t j = 0; j < tmp[i].size(); j++) {
            this->ui->tableWidget->setItem(i - 1, j, new QTableWidgetItem(QString::fromStdString(tmp[i][j])));

        }
    }
    this->ui->spinBox->setValue(this->ui->tableWidget->model()->rowCount());
    this->setModal(true);
    this->exec();
}

Table::~Table() {
    this->balans = nullptr;
    delete ui;
}

void Table::on_pushButton_clicked() {
    std::vector<std::vector<std::string> > data;
    data.push_back(this->rootTable);
    for(int i = 0; i < this->ui->tableWidget->model()->rowCount(); i++) {
         std::vector<std::string> data1;
         for (int j = 0; j < this->ui->tableWidget->columnCount(); j++) {
               if (ui->tableWidget->item(i, j) == nullptr && i <= notVoidElementTabel) {
                   QMessageBox::warning(this, "Error", "Cells must not be empty!!!");
                   return;
               }
               data1.push_back(ui->tableWidget->item(i, j)->text().toStdString());
         }
         data.push_back(data1);
    }
    if ( !this->balans->saveVector(data, openWindow) ) {
        QMessageBox::warning(this, "Error", "Erroneously from the field not all are\n found or the numbers are not in the drains with the numbers of available characters!!!");
    }
}

void Table::on_pushButton_2_clicked() {
    this->ui->spinBox->setMaximum(ui->tableWidget->rowCount() + 1);
    this->ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    this->ui->spinBox->setValue(this->ui->tableWidget->model()->rowCount());
}


void Table::on_pushButton_3_clicked() {
    this->ui->tableWidget->removeRow(this->ui->spinBox->value() - 1);
    this->ui->spinBox->setMaximum(this->ui->tableWidget->model()->rowCount());
    this->ui->spinBox->setValue(this->ui->tableWidget->model()->rowCount());
}

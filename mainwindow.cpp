#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    this->Balans = nullptr;
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete Balans;
    delete ui;
}

bool MainWindow::getStatusBalance()
{
    if (!this->Balans) {
        QMessageBox::warning(this, "Error", "Select file to get values");
        return false;
    }
    return true;
}

void MainWindow::on_pushButton_clicked()
{
    std::string str = QFileDialog::getExistingDirectory().toStdString();
    Balans = new myClass::balance(str);
    if ( Balans->initDatabase() )
    {
        ui->pushButton->setEnabled(false);
        ui->pushButton->hide();
        ui->label_2->setText("Answer: click on knrpu and here will be the answer!!!");
        ui->label_11->setText("Answer: click on knrpu and here will be the answer!!!");
        ui->label_16->setText("Answer: click on knrpu and here will be the answer!!!");
        ui->label_23->setText("Answer: click on knrpu and here will be the answer!!!");
        ui->label_51->setText("Answer: click on knrpu and here will be the answer!!!");
        return;
    }
    QMessageBox::warning(this, "Error", "There are no files in the folder!!!\nTry again!!!");
    delete Balans;
    Balans = nullptr;
}


void MainWindow::on_pushButton_4_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_1->text().toStdString();
    std::string instity = ui->lineEdit_2->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    if ( !instity.size() )
    {
         QMessageBox::warning(this, "Error", "Instityte field should not be empty!!!");
         return;
    }
    bool getInst = Balans->getInstitut(city, instity);
    ui->label_2->setText(getInst ? "Answer: There is a univercity!!!" : "Answer: no univercity!!!");
}


void MainWindow::on_pushButton_2_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_1->text().toStdString();
    std::string instity = ui->lineEdit_2->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    if ( !instity.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not instityte must not be empty!!!");
        return;
    }
    uint lunchInstit = this->Balans->getInstituteDinnerCost(city, instity);
    ui->label_2->setText("Answer: average cost of lunch at a given univercity, " + QString::number(lunchInstit) + "!");
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->lineEdit_1->clear();
    ui->lineEdit_2->clear();
}


void MainWindow::on_pushButton_14_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_5->text().toStdString();
    std::string addres = ui->lineEdit_7->text().toStdString();
    std::string instity = ui->lineEdit_8->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    if ( !addres.size() )
    {
        QMessageBox::warning(this, "Error", "Home addrecfield should not be empty!!!");
        return;
    }
    if ( !instity.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not instityte must not be empty!!!");
         return;
    }
    int lunchInstit = this->Balans->getTransportCost(city, addres, instity);
    ui->label_11->setText("Answer: minimum price for transport, " + QString::number(lunchInstit) + "!");
}


void MainWindow::on_pushButton_17_clicked()
{
    ui->lineEdit_5->clear();
    ui->lineEdit_7->clear();
    ui->lineEdit_8->clear();
}


void MainWindow::on_pushButton_13_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_5->text().toStdString();
    std::string instity = ui->lineEdit_8->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !instity.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not instityte must not be empty!!!");
         return;
    }
    bool getInst = this->Balans->getInstitut(city, instity);
    ui->label_11->setText(getInst ? "Answer: There is a univercity!!!" : "Answer: no univercity!!!");
}


void MainWindow::on_pushButton_15_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_5->text().toStdString();
    std::string addres = ui->lineEdit_7->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !addres.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not address must not be empty!!!");
         return;
    }
    bool getAddr = this->Balans->getAddress(city, addres);
    ui->label_11->setText(getAddr ? "Answer: there is an address!!!" : "Answer: there is no address!!!");

}


void MainWindow::on_pushButton_23_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    std::string coffee = ui->lineEdit_9->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !coffee.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not coffee must not be empty!!!");
         return;
    }
    bool getCoffe = this->Balans->getCoffe(city, coffee);
    ui->label_16->setText(getCoffe ? "Answer: such coffee is!!!" : "Answer: mute coffee!!!");
}

void MainWindow::on_pushButton_21_clicked()
{
    ui->lineEdit_6->clear();
    ui->lineEdit_9->clear();
    ui->lineEdit_10->clear();
}


void MainWindow::on_pushButton_24_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    std::string cinema = ui->lineEdit_10->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !cinema.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not cinema must not be empty!!!");
        return;
    }
    bool getCinema = this->Balans->getCinema(city, cinema);
    ui->label_16->setText(getCinema ? "Answer: such cinema is!!!" : "Answer: mute cinema!!!");
}


void MainWindow::on_pushButton_19_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    std::string cinema = ui->lineEdit_10->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !cinema.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not cinema must not be empty!!!");
        return;
    }
    uint score = this->Balans->getCinemaCost(city, cinema);
    ui->label_16->setText("Answer: such a bill for a ticket in cinema " +  QString::number(score) + "!!!");
}



void MainWindow::on_pushButton_20_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    std::string coffe = ui->lineEdit_9->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !coffe.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not coffee must not be empty!!!");
        return;
    }
    uint score = this->Balans->getCoffeeCost(city, coffe);
    ui->label_16->setText("Answer: such a bill for a ticket in coffe " +  QString::number(score) + "!!!");
}

void MainWindow::on_pushButton_25_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_1->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    bool cityFind = this->Balans->isCity(city);
    ui->label_2->setText(cityFind ? "Answer: such a city exists!!!" : "Answer:  this city is not in the database!!!");
}

void MainWindow::on_pushButton_26_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_5->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    bool cityFind = this->Balans->isCity(city);
    ui->label_11->setText(cityFind ? "Answer: such a city exists!!!" : "Answer:  this city is not in the database!!!");
}

void MainWindow::on_pushButton_27_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    bool cityFind = this->Balans->isCity(city);
    ui->label_16->setText(cityFind ? "Answer: such a city exists!!!" : "Answer:  this city is not in the database!!!");
}


void MainWindow::on_pushButton_18_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_6->text().toStdString();
    std::string coffe = ui->lineEdit_9->text().toStdString();
    std::string cinema = ui->lineEdit_10->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !coffe.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not coffee must not be empty!!!");
        return;
    }
    if ( !cinema.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not cinema must not be empty!!!");
        return;
    }
    uint score = this->Balans->getWeekandCost(city, cinema, coffe);
    ui->label_16->setText("Total movie and coffee costs per month " + QString::number(score) + "!!!");
}


void MainWindow::on_pushButton_38_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_11->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    bool cityFind = this->Balans->isCity(city);
    ui->label_23->setText(cityFind ? "Answer: such a city exists!!!" : "Answer:  this city is not in the database!!!");
}


void MainWindow::on_pushButton_36_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_11->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    int age = ui->spinBox->value();
    uint other = this->Balans->getOtherMontlyCosts(city, age);
    ui->label_23->setText("Answer: get other montly costs " + QString::number(other) + "!!!");
}


void MainWindow::on_pushButton_29_clicked()
{
    ui->lineEdit_11->clear();
    ui->spinBox->setValue(0);
}


void MainWindow::on_pushButton_37_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_11->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    uint sum = this->Balans->getHomeFoodCost(city);
    ui->label_23->setText("Answer: food spending in the region " + QString::number(sum) + "!!!");
}


void MainWindow::on_pushButton_68_clicked()
{
    ui->lineEdit_22->clear();
    ui->lineEdit_25->clear();
    ui->lineEdit_24->clear();
    ui->lineEdit_23->clear();
    ui->lineEdit_40->clear();
    ui->spinBox_6->setValue(0);
    ui->spinBox_7->setValue(1);
}


void MainWindow::on_pushButton_88_clicked()
{
    if ( !this->getStatusBalance() ) {
        return ;
    }
    std::string city = ui->lineEdit_22->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city city must not be empty!!!");
        return;
    }
    bool cityFind = this->Balans->isCity(city);
    ui->label_51->setText(cityFind ? "Answer: such a city exists!!!" : "Answer:  this city is not in the database!!!");
}


void MainWindow::on_pushButton_89_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_22->text().toStdString();
    std::string addres = ui->lineEdit_25->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !addres.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not address must not be empty!!!");
         return;
    }
    bool getAddr = this->Balans->getAddress(city, addres);
    ui->label_51->setText(getAddr ? "Answer: there is an address!!!" : "Answer: there is no address!!!");
}


void MainWindow::on_pushButton_90_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_22->text().toStdString();
    std::string instity = ui->lineEdit_24->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !instity.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not instityte must not be empty!!!");
         return;
    }
    bool getInst = this->Balans->getInstitut(city, instity);
    ui->label_51->setText(getInst ? "Answer: There is a univercity!!!" : "Answer: no univercity!!!");
}



void MainWindow::on_pushButton_91_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_22->text().toStdString();
    std::string cinema = ui->lineEdit_23->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !cinema.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not cinema must not be empty!!!");
        return;
    }
    bool getCinema = this->Balans->getCinema(city, cinema);
    ui->label_51->setText(getCinema ? "Answer: such cinema is!!!" : "Answer: mute cinema!!!");
}


void MainWindow::on_pushButton_92_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_23->text().toStdString();
    std::string coffee = ui->lineEdit_40->text().toStdString();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !coffee.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not coffee must not be empty!!!");
         return;
    }
    bool getCoffe = this->Balans->getCoffe(city, coffee);
    ui->label_51->setText(getCoffe ? "Answer: such coffee is!!!" : "Answer: mute coffee!!!");
}


void MainWindow::on_pushButton_93_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    int mount = ui->spinBox_7->value();
    mount = this->Balans->getWorkdays(mount);
    ui->label_51->setText("Answer: there are so many working days in such a month " + QString::number(mount) + "!!!");
}


void MainWindow::on_pushButton_94_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    std::string city = ui->lineEdit_23->text().toStdString();
    std::string addres = ui->lineEdit_25->text().toStdString();
    std::string instity = ui->lineEdit_24->text().toStdString();
    std::string cinema = ui->lineEdit_23->text().toStdString();
    std::string coffee = ui->lineEdit_40->text().toStdString();
    int age = ui->spinBox_6->value();
    int mount = ui->spinBox_7->value();
    if ( !city.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not city must not be empty!!!");
        return;
    }
    if ( !addres.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not address must not be empty!!!");
         return;
    }
    if ( !instity.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not instityte must not be empty!!!");
         return;
    }
    if ( !cinema.size() )
    {
        QMessageBox::warning(this, "Error", "Field must not not cinema must not be empty!!!");
        return;
    }
    if ( !coffee.size() )
    {
         QMessageBox::warning(this, "Error", "Field must not not coffee must not be empty!!!");
         return;
    }
    uint getCost = this->Balans->getCosts(mount, city, addres, instity, cinema, coffee, age);
    ui->label_51->setText("Answer: total costs " + QString::number(getCost) + "!!!");
}

void MainWindow::on_pushButton_28_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    this->tab = new Table();
    this->tab->exitTable(this->Balans, myClass::tableEn::COSTS);
    delete this->tab;
}


void MainWindow::on_pushButton_3_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    this->tab = new Table();
    this->tab->exitTable(this->Balans, myClass::tableEn::INSTITYT);
    delete this->tab;
}


void MainWindow::on_pushButton_16_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    this->tab = new Table();
    this->tab->exitTable(this->Balans, myClass::tableEn::TRANSPORT);
    delete this->tab;
}


void MainWindow::on_pushButton_22_clicked()
{
    if ( !this->getStatusBalance() )
    {
        return ;
    }
    this->tab = new Table();
    this->tab->exitTable(this->Balans, myClass::tableEn::ENTERTAINMENT);
    delete this->tab;
}


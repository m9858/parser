#include <balance.h>

#include <fstream>
#include <cmath>
#include <set>
#include <stdio.h>

static bool getNumber(std::string& number) {
    for (size_t i = 0; i < number.size(); i++) {
        if ( !('0' <= number[i] && number[i] <= '9') ) {
        	    return false;
        }
    }
    return true;
}

static bool voidArray(std::vector<std::string>& a) {
    for (size_t i = 0 ; i < a.size(); i++) {
        if ( a[i].size() ) {
            return false;
        }
    }
    return true;
}


//!  функция для открытия и создания массива
static std::vector<std::vector<std::string> > readField(std::ifstream& fin) {
	std::vector<std::vector<std::string> > tmpArr;
	size_t j = 0;
	std::string line;
  	while(getline(fin, line, '\r')) {
        	if  ( line.size() == 1 && line[0] == '\n') {
            		continue;
       		 }
		tmpArr.push_back(std::vector<std::string>());
		std::string str = "";
		for (size_t i = 0; i < line.size(); i++) {
			if (line[i] == ',') {
				tmpArr[j].push_back(str);
				str = "";
			}
			else{
				str = (line[i] != '\n' ? str + line[i] : str);
			}
		}
		tmpArr[j].push_back(str);
		j += 1;
    }
	return tmpArr;
}

//! конструктор
myClass::balance::balance(std::string filder) : _filder(filder) {
    if (_filder.size() != 0 && _filder[_filder.size() - 1] != '/') {
        _filder += '/';
    }
}

//! Инициализация базы данных
bool myClass::balance::initDatabase() {
    std::ifstream fin[4];
	
	fin[0].open(_filder + "Caffe-and-cinema.csv");
	fin[1].open(_filder + "Costs.csv");
	fin[2].open(_filder + "Institute.csv");
	fin[3].open(_filder + "Transport.csv");
	for (size_t i = 0; i < sizeof(fin) / sizeof(std::ifstream); i++) {
		if (!fin[i].is_open()) {
			return false;
		}
	}
	caffeCinema = readField(fin[0]);
	costs = readField(fin[1]);
	institute = readField(fin[2]);
	transport = readField(fin[3]);
    for (size_t i = 0; i < 4; i++) {
		fin[i].close();
	}
	return true;
}

//! Получаем из базы данных средние по региону затраты в данном месяце 
//!     для людей данного возраста
//! (по остальным статьям кроме еды, транспорта и развлечений) 
uint myClass::balance::getOtherMontlyCosts(const std::string& city, uint age) const {
	size_t cityIndex = 0;
	size_t ageIndex = 0;
	size_t costsIndex = 0;
	for (size_t i = 0; i < costs[0].size(); i++) {
		if ( costs[0][i] == "City" ) {
			cityIndex = i;
		}
		else if ( costs[0][i] == "Age" ) {
			ageIndex = i;
		}
		else if ( costs[0][i] == "Other costs" ) {
			costsIndex = i;
		}
	}
	for (size_t i = 1; i < costs.size(); i++) {
        if ( costs[i][cityIndex] == city && std::stoi(costs[i][ageIndex]) == static_cast<int>(age)) {
			return std::stoi(costs[i][costsIndex]);
		}
	}
	return 0;
} 

//! Получаем из базы данных среднюю стоимость обеда в столовой данного института
uint myClass::balance::getInstituteDinnerCost(const std::string& city, const std::string& institute) const {
	size_t instityIndex = 0;
	size_t citeIndex = 0;
	size_t dinnerIndex = 0;
	for (size_t i = 0; i < this->institute[0].size(); i++) {
		if ( this->institute[0][i] == "City" ) {
			citeIndex = i;
		}
		else if ( this->institute[0][i] == "Institute" ) {
			instityIndex = i;
		}
		else if ( this->institute[0][i] == "Dinner cost" ) {
			dinnerIndex = i;
		}
	}
	for (size_t i = 1; i < this->institute.size(); i++) {
		if ( this->institute[i][citeIndex] == city && this->institute[i][instityIndex] == institute) {
			return std::stoi(this->institute[i][dinnerIndex]); 
		}
	}
    return 0;
}

//! Получаем из базы данных стоимость билета на вечерний сеанс в данном кинотетре
uint myClass::balance::getCinemaCost(const std::string& city, const std::string& cinema) const {
	size_t cityIndex = 0;
        size_t cinemaIndex = 0;
        size_t scoreIndex = 0;
	for (size_t i = 0; i < caffeCinema[0].size(); i++) {
		if (caffeCinema[0][i] ==  "City") {
				cityIndex = i;
		}
		else if (caffeCinema[0][i] == "Cinema") {
				cinemaIndex = i;
		}
		else if (caffeCinema[0][i] == "Cinema cost") {
			    	scoreIndex = i;
		}
	}
	for (size_t i = 1; i < caffeCinema.size(); i++) {
		if ( caffeCinema[i][cityIndex] == city &&  caffeCinema[i][cinemaIndex] == cinema ) {
			return std::stoi(caffeCinema[i][scoreIndex]);
		}
	}
	return 0;
}

//!  Получаем из базы данных средний чек в данном баре
uint myClass::balance::getCoffeeCost(const std::string& city, const std::string& coffee) const{
	size_t cityIndex = 0;
	size_t coffeeIndex = 0;
	size_t scoreIndex = 0;
        for (size_t i = 0; i < caffeCinema[0].size(); i++) {
		if (caffeCinema[0][i] == "City") {
				cityIndex = i;
		}
		else if (caffeCinema[0][i] == "Caffe") {
				coffeeIndex = i;
		}
		else if (caffeCinema[0][i] == "Average caffe cost") {
			    	scoreIndex = i;
		}
	}
	for (size_t i = 1; i < caffeCinema.size(); i++) {
		if ( caffeCinema[i][cityIndex] == city &&  caffeCinema[i][coffeeIndex] == coffee ) {
			return std::stoi(caffeCinema[i][scoreIndex]);
		}
	}
	return 0;
}

//! Получаем из базы данных средние по региону затраты на еду
uint myClass::balance::getHomeFoodCost(const std::string& city) const {
	size_t cityIndex = 0;
	size_t averageIndex = 0;
	for (size_t i = 0; i < costs[0].size(); i++) {
		if (costs[0][i] == "City") {
		    cityIndex = i;
		}
		else if (costs[0][i] == "Average food cost per month") {
				averageIndex = i;
		}
	}
	size_t sum = 0, count = 0;
	for (size_t i = 1; i < costs.size(); i++) {
		if ( costs[i][cityIndex] == city) {
			sum += std::stoi(costs[i][averageIndex]);
			count += 1;
		}
	}
	return sum ? sum / count : 0;
}

//! По выходным дням расходы складываются из стоимости:
//! - похода в кино (пешком)
//! - похода  в кафе (пешком)
//! - завтрака и обеда дома 
uint myClass::balance::getWeekandCost(const std::string& city, const std::string& cinema,
                    const std::string& coffee)  const {
    return static_cast<uint>(0.66 * this->getHomeFoodCost(city) + 
        this->getCinemaCost(city, cinema) + this->getCoffeeCost(city, coffee) + 0.5);
}

//! Получаем из базы данных стоимость кратчайшей дороги до инстиутта
uint myClass::balance::getTransportCost(const std::string& city, const std::string& homeAddress, const std::string& institute) const {
     size_t cityIndex = 0;
     size_t instituteIndex = 0;
     size_t homeAddressIndex = 0;
     size_t timeIndex = 0;
     for (size_t i = 0; i < transport[0].size(); i++) {
	     if (transport[0][i] == "City") {
			     cityIndex = i;
	     }
	     else if (transport[0][i] == "District") {
			     homeAddressIndex = i;
	     }
             else if (transport[0][i] == "Transport cost") {
			     timeIndex = i;
	     }
	     else if (transport[0][i] == "Institute") {
			     instituteIndex = i;
	     } 
     }
     uint max = 0;
     for (size_t i = 1; i < transport.size(); i++) {
	     if (city == transport[i][cityIndex] && 
	         homeAddress == transport[i][homeAddressIndex] &&
		 institute == transport[i][instituteIndex]) {
		     max = std::max(static_cast<int>(max), std::stoi(transport[i][timeIndex]));
	     }
     }
     return max ? max : 0 ;
}

//!По выходным дням расходы складываются из стоимости:
//!- похода в кино (пешком)
//!- похода  в кафе (пешком)
//!- завтрака и обеда дома 
uint myClass::balance::getWorkdayCost(const std::string& city, const std::string& homeAddress,
                    const std::string& institute) const {
    return static_cast<uint>(2 * this->getTransportCost(city, homeAddress, institute) +
        this->getInstituteDinnerCost(city, institute) + 0.66 * this->getHomeFoodCost(city) + 0.5);
}

//!Расходы:
//!- по рабочим дням
//!- по выходным дням
//!- прочие расходы
uint myClass::balance::getCosts(uint month, const std::string& city, const std::string& homeAddress, 
              const std::string& institute, const std::string& cinema,
              const std::string& coffee, uint age) const {

    const uint daysCount = getDaysCount(month);
    const uint workDays = getWorkdays(month);
    const uint weekends = daysCount - workDays;

    return workDays * getWorkdayCost(city, homeAddress, institute) +
           weekends * getWeekandCost(city, cinema, coffee) + getOtherMontlyCosts(city, age);
}

//! Вычисляем количество дней в месяце
uint myClass::balance::getDaysCount(uint month) const {
	return month == 2 ? 28 : 30 + ( month + month / 8) % 2;
}

//! Получаем из базы данных число рабочих дней данном в месяце
uint myClass::balance::getWorkdays(uint month) const {
    if ( !(1 <= month  && month  <= 12) ) {
        return 0;
    }
    return this->workday[month - 1];
}

//! Проверка етли такой инстут в бд 
bool myClass::balance::getInstitut(const std::string& city, const std::string& instity) const {
	size_t cityIndex = 0;
	size_t instIndex = 0;
	for (size_t i = 0; i < this->institute[0].size(); i++) {
		if ( this->institute[0][i] == "City" ) {
			cityIndex = i;
		}
		else if ( this->institute[0][i] == "Institute" ) {
			instIndex = i;
		}
	}
	for (size_t i = 1; i < this->institute.size(); i++) {
		if ( this->institute[i][cityIndex] == city && 
		     this->institute[i][instIndex] == instity ) {
		       return true;
		} 
	}
	for (size_t i = 0; i < this->transport[0].size(); i++) {
		if ( this->transport[0][i] == "City" ) {
			cityIndex = i;
		}
		else if ( this->transport[0][i] == "Institute" ) {
			instIndex = i;
		}
	}
	for (size_t i = 1; i < this->transport.size(); i++) {
		if ( this->transport[i][cityIndex] == city && 
		     this->transport[i][instIndex] == instity ) {
		       return true;
		} 
	}


	return false;
}	

//! Проверка естли такой кинотиатор
bool myClass::balance::getCinema(const std::string& city, const std::string& cinema) const {
	size_t cityIndex = 0;
	size_t cineIndex = 0;
	for (size_t i = 0; i < this->caffeCinema[0].size(); i++) {
		if ( this->caffeCinema[0][i] == "City" ) {
			cityIndex = i;
		}
		else if ( this->caffeCinema[0][i] == "Cinema" ) {
			 cineIndex = i;
		}
	}
	for (size_t i = 1; i < this->caffeCinema.size(); i++) {
		if ( this->caffeCinema[i][cityIndex] == city && 
		     this->caffeCinema[i][cineIndex] == cinema) {
		       return true;
		} 
	}
	return false;
}

//! проверка естли такое кофе 
bool myClass::balance::getCoffe(const std::string& city, const std::string& coffe) const {
	size_t cityIndex = 0;
	size_t coffIndex = 0;
	for (size_t i = 0; i < this->caffeCinema[0].size(); i++) {
		if ( this->caffeCinema[0][i] == "City" ) {
			cityIndex = i;
		}
		else if ( this->caffeCinema[0][i] == "Caffe" ) {
			 coffIndex = i;
		}
	}
	for (size_t i = 1; i < this->caffeCinema.size(); i++) {
		if ( this->caffeCinema[i][cityIndex] == city && 
		     this->caffeCinema[i][coffIndex] == coffe ) {
		       return true;
		} 
	}
	return false;
}

//! Проверка может ли создавать такой месяу
bool myClass::balance::isMonth(int month) const {
	return 0 < month && month < 13;
}

//! Ввод месица
uint myClass::balance::getMonth() const {
    uint other;
    std::cout << "Input: mounth: ";
    std::cin >> other;
    while (!this->isMonth(other)) {
		std::cout << "Error: not this mounth\nTry again: ";
		std::cin >> other;
    }
    return other;    
}

//!  Ввод года раждения
uint myClass::balance::getAge() const{
	uint other;
	std::cout << "Input: Age: ";
	std::cin >> other;
	while ( !(0 < other && other <= 128)) {
		std::cout << "Error: not this Age\nTry again: ";
		std::cin >> other;
    	}
   	return other; 
}

//! Проверка если такой город
bool myClass::balance::isCity(const std::string& city) const{
	size_t cityIndex = 0;
	for (size_t i = 0;i < caffeCinema[0].size(); i++) {
		if (caffeCinema[0][i] == "City" ) {
			cityIndex = i;
			break;
		}
	}
	for (size_t i = 1; i < caffeCinema.size(); i++) {
		if (caffeCinema[i][cityIndex] == city) {
			return true;
		}
	}
	
	for (size_t i = 0;i < costs[0].size(); i++) {
		if ( costs[0][i] == "City" ) {
			cityIndex = i;
			break;
		}
	}
	for (size_t i = 1; i < costs.size(); i++) {
		if (costs[i][cityIndex] == city) {
			return true;
		}
	}

	for (size_t i = 0;i < institute[0].size(); i++) {
		if ( institute[0][i] == "City" ) {
			cityIndex = i;
			break;
		}
	}
	for (size_t i = 1; i < institute.size(); i++) {
		if (institute[i][cityIndex] == city) {
			return true;
		}
	}
	for (size_t i = 0;i < transport[0].size(); i++) {
		if ( transport[0][i] == "City" ) {
			cityIndex = i;
			break;
		}
	}
	for (size_t i = 1; i < transport.size(); i++) {
		if (transport[i][cityIndex] == city) {
			return true;
		}
	}
	return false;
}

//! Ввод города
std::string myClass::balance::getCity() const{
	std::string other;
	std::cout << "Input: city: ";
	std::cin >> other;
	while (!this->isCity(other)) {
			std::cout << "Error: not this city in bd\nTry again: ";
			std::cin >> other;
	}
	return other;    
}

//! Проверка адреса
bool myClass::balance::getAddress(const std::string& city, const std::string& address) const{
	size_t cityIndex = 0;
	size_t addrIndex = 0;
	for (size_t i = 0;i < caffeCinema[0].size(); i++) {
		if ( caffeCinema[0][i] == "City" ) {
			cityIndex = i;
		}
		else if (caffeCinema[0][i] == "Address" ) {
			addrIndex = i;
		}


	}
	for (size_t i = 1; i < caffeCinema.size(); i++) {
		if (caffeCinema[i][cityIndex] == city && caffeCinema[i][addrIndex] == address) { 
			return true;
		}
	}
	for (size_t i = 0;i < transport[0].size(); i++) {
		if ( transport[0][i] == "City" ) {
			cityIndex = i;
		}
		else if (transport[0][i] == "Address" ) {
			addrIndex = i;
		}


	}
	for (size_t i = 1; i < transport.size(); i++) {
		if (transport[i][cityIndex] == city && transport[i][addrIndex] == address) { 
			return true;
		}
	}
	return false;
}

const std::vector<std::vector<std::string> >& myClass::balance::getArrCafeCinema() const {
    return this->caffeCinema;
}

const std::vector<std::vector<std::string> >& myClass::balance::getArrCosts() const {
    return this->costs;
}

const std::vector<std::vector<std::string> >& myClass::balance::getArrInstitute() const {
    return this->institute;
}
const std::vector<std::vector<std::string> >& myClass::balance::getArrTransport() const {
    return this->transport;
}

bool myClass::balance::saveVector(std::vector<std::vector<std::string> >& data, myClass::tableEn newWind) {
    std::ofstream fout;
    size_t end = 0;
    std::set<size_t> setNumd;
    for (size_t i = 0; i < data[0].size() && data[0][i].size() ; i++) {
        if ( data[0][i] == "Age" || data[0][i].find("cost") < data[0][i].size()) {
           setNumd.insert(i);
        }
        end = i;
    }
    end += 1;
    std::string str = "";
    for (size_t i = 0; i < data.size(); i++) {
          if (voidArray(data[i])) {
              continue;
          }
          for (size_t j = 0; j < data[0].size(); j++) {
              if (!data[i][j].size()  && j < end ) {
                  return false;
              }
              if ( i != 0 ) {
                    if ( setNumd.count(j) && !getNumber(data[i][j])) {
                        return false;
                    }
              }
              str += data[i][j] + ( j + 1 == data[i].size() ? "\r\n" : ",");
          }
    }
    switch(newWind) {
        case (myClass::tableEn::COSTS):
            this->costs = data;
            fout.open(this->_filder + "Costs.csv",  std::ofstream::out);
            break;

        case (myClass::tableEn::INSTITYT):
            this->institute = data;
            fout.open(this->_filder + "Institute.csv",  std::ofstream::out);
            break;

        case(myClass::tableEn::TRANSPORT):
            this->transport = data;
            fout.open(this->_filder + "Transport.csv",  std::ofstream::out);
            break;
        case(myClass::tableEn::ENTERTAINMENT):
            this->caffeCinema = data;
            fout.open(this->_filder + "Caffe-and-cinema.csv",  std::ofstream::out);
            break;
    }
    fout << str;
    fout.close();
    return true;
}

